require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) { |acc, num| acc + num }
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? do |words|
    contain_substring?(words, substring)
  end
end

def contain_substring?(words, substring)
  words.split(" ").any? { |word| word.include?(substring) }
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  string = string.split(" ").join.split("")
  unique = string.uniq

  unique.select { |letter| string.count(letter) > 1 }.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  words = string.split(' ')
  words.sort_by { |word| word.length }[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ("a".."z").reject { |letter| string.include?(letter) }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select { |year| not_repeat_year?(year) }
end

def not_repeat_year?(year)
  year = year.to_s.split('')
  year == year.uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs_copy = songs[0..-1]

  songs_copy.each_with_index do |song_name, idx|
    if repeats?(song_name, songs_copy[idx..-1])
      songs_copy.delete(song_name)
    end
  end

  songs_copy.uniq
end


def repeats?(song_name, songs)
  index = songs.index(song_name)
  right_neighbor = index + 1


  if songs[right_neighbor] != nil && song_name == songs[right_neighbor]
    return true
  else
    return false
  end
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  closest_distance = Float::INFINITY
  closest_word = nil

  string.split(" ").each do |word|
    word = remove_punctuation(word)
    last_c_index_in_word = c_distance(word)

    if last_c_index_in_word != nil && last_c_index_in_word < closest_distance
      closest_distance = last_c_index_in_word
      closest_word = word.join('')
    end
  end

  closest_word
end

def c_distance(word)
  word.reverse.index("c")
end

def remove_punctuation(word)
  word.split("").select { |ch| ch if /[A-Za-z0-9]/.match(ch) }
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  repeating_ranges = []

  arr.each_with_index do |num, idx|
    idx2 = idx
    num_streak = [idx]

    while idx2 < arr.length - 1
      idx2 += 1
      if num != arr[idx2]
        break
      else
        num_streak << idx2
      end
    end

    if num_streak.length > 1
      repeating_ranges << [ num_streak.first, num_streak.last ]
    end
  end

  remove_overlapping_range(repeating_ranges)
end


def remove_overlapping_range(repeating_ranges)
    repeating_ranges.each_with_index do |pair, idx|
      if idx != 0 && repeating_ranges[idx][0].between?(repeating_ranges[idx - 1].first, repeating_ranges[idx - 1].last)
        repeating_ranges.delete_at(idx)
      end
    end
end
